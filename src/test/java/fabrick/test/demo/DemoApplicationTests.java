package fabrick.test.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import fabrick.test.demo.controller.AccountController;


@SpringBootTest
class DemoApplicationTests {
//	
	@Autowired
	AccountController controller;


	@Test
	void getBalance() throws Exception {
		controller.getBalance(14537780l);
	}
	
	@Test
	void getTransactions() throws Exception {
		controller.getAccountTransaction(14537780l,"2019-01-01","2019-12-01");
	}
//	
	@Test
	void moneyTransfer() throws Exception {
		controller.moneyTransfer(14537780l,1l);
	}

}
