package fabrick.test.demo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AccountController {

	@Value("${domain.default}")
	private String domainDefault;

	@Value("${apikey}")
	private String apikey;

	@GetMapping(value = "/accounts/{accountId}/balance", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Response getBalance(@PathVariable Long accountId) {
		int responseCode = 200;
		StringBuffer content = new StringBuffer();
		try {
			URL url = new URL(domainDefault + "/api/gbs/banking/v4.0/accounts/" + accountId + "/balance");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			setHeadersConnection(con);
			responseCode = con.getResponseCode();

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();
			con.disconnect();
		} catch (ProtocolException ex) {
			 return Response.serverError().entity(ex.getMessage()).build();
		} catch (IOException ex) {
			 return Response.serverError().entity(ex.getMessage()).build();
		}
		 return Response.status(responseCode).entity(content.toString()).build();
	}

	@GetMapping(value = "/accounts/{accountId}/transactions", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Response getAccountTransaction(@PathVariable Long accountId,
			@RequestParam(name = "fromAccountingDate", required = true) String fromAccountingDate,
			@RequestParam(name = "toAccountingDate", required = true) String toAccountingDate) {
		
		StringBuffer content = new StringBuffer();
		int responseCode = 200;
		try {
			URL url = new URL(
					domainDefault + "/api/gbs/banking/v4.0/accounts/" + accountId + "/transactions?fromAccountingDate="
							+ fromAccountingDate + "&toAccountingDate=" + toAccountingDate);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			setHeadersConnection(con);
			responseCode = con.getResponseCode();
			

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
	
			while ((inputLine = in.readLine()) != null) {
				content.append(inputLine);
			}
			in.close();
			con.disconnect();
		} catch (ProtocolException ex) {
			 return Response.serverError().entity(ex.getMessage()).build();
		} catch (IOException ex) {
			 return Response.serverError().entity(ex.getMessage()).build();
		}
		return Response.status(responseCode).entity(content.toString()).build();
	}

	@PostMapping(value = "/accounts/{accountId}/payments/money-transfers", produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public Response moneyTransfer(@PathVariable Long accountId, @RequestBody Object body) {
		int responseCode = 200;
		try {
			URL url = new URL(
					domainDefault + "/api/gbs/banking/v4.0/accounts/" + accountId + "/payments/money-transfers");

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			setHeadersConnection(con);
			con.setDoOutput(true);
			con.setDoInput(true);
			responseCode = con.getResponseCode();
			
			String jsonInputString = body.toString();
			try (OutputStream os = con.getOutputStream()) {
				byte[] input = jsonInputString.getBytes("utf-8");
				os.write(input, 0, input.length);
			}
			
			StringBuilder response = new StringBuilder();
			
			try (BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
				String responseLine = null;
				while ((responseLine = br.readLine()) != null) {
					response.append(responseLine.trim());
				}
			}
			con.disconnect();
			return Response.status(responseCode).entity(response.toString()).build();
		} catch (ProtocolException ex) {
			return Response.serverError().entity(ex.getMessage()).build();
		} catch (IOException ex) {
			return Response.serverError().entity(ex.getMessage()).build();

		}
	}

	private void setHeadersConnection(HttpURLConnection con) {
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestProperty("Auth-Schema", "S2S");
		con.setRequestProperty("apikey", apikey);
		con.setRequestProperty("X-Time-Zone", "Europe/Rome");
	}

}
